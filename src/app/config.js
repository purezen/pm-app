export const intialParentKey = 'ekjt3';

export const initialItemList = [
    { id: "ekjt1", text: "Todo", parentKey: null },
    { id: "ekjt2", text: "clean laptop", parentKey: "ekjt1" },
    { id: "ekjt3", text: "get groceries", parentKey: "ekjt1" },
    { id: "ekjt4", text: "apples", parentKey: "ekjt3" }
];
