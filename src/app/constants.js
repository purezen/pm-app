// key codes
export const keyCodeForReturnKey = 13;

// default data
export const defaultBlockContent = { "depth": 0, "type": "unordered-list-item", "inlineStyleRanges": [], "entityRanges": [], "data": {} };
