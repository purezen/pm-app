import React, { Component } from 'react';

import MainEditor from './components/Editor';
import Breadcrumb from './components/Breadcrumb';

import { SmallBox } from './components/utils';

import { generatePathArray } from './utils/utils';

import { initialItemList, intialParentKey } from './config';

import './App.css';
import { appStyle } from './styles';

class App extends Component {
  constructor() {
    super();
    this.state = {
      itemList: initialItemList,
      parentKey: intialParentKey,
      editorItemList: this._calculateEditorItemList(initialItemList, intialParentKey)
    }

    this.bindFunctionsToClassThis();
  }

  updateEditorItemList(editorItemList) {
    this.setState({ editorItemList });
  }

  setParentKey(parentKey) {
    const { itemList } = this.state;
    const editorItemList = this._calculateEditorItemList(itemList, parentKey);
    this.setState({ parentKey, editorItemList });
  }

  _calculateEditorItemList(itemList, parentKey) {
    return itemList.filter(i => i.parentKey === parentKey);
  }

  render() {
    const { itemList, editorItemList, parentKey } = this.state;
    const pathArray = generatePathArray(itemList, parentKey)

    return (
      <div style={appStyle}>
        <Breadcrumb
          pathArray={pathArray}
          setParentKey={this.setParentKey} />
        <SmallBox />
        <MainEditor
          itemList={editorItemList}
          updateEditorItemList={this.updateEditorItemList} />
      </div>
    )
  }

  bindFunctionsToClassThis() {
    // binding because of es6 behaviour
    this._calculateEditorItemList = this._calculateEditorItemList.bind(this);
    this.setParentKey = this.setParentKey.bind(this)
    this.updateEditorItemList = this.updateEditorItemList.bind(this);
  }
};

export default App;
