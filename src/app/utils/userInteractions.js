import {
    convertToRaw, convertFromRaw, EditorState, SelectionState, Modifier
} from 'draft-js';

export const setHighlightBlockHandle = (e, highlightStateIsOn, editorState, setState) => {
    // sets block where cursor is hovering so that menu can be shown
    // note: this might have perf issue since it toggles over a single block element too.. ie. li -> div -> li
    const targetEl = e.target;
    const elAttrNames = targetEl.getAttributeNames();
    if (elAttrNames.includes('data-offset-key')) {
        // refactor using immutable api? last time it didn't work
        const contentBlocks = convertToRaw(editorState.getCurrentContent()).blocks;
        const contentBlockKeys = contentBlocks.map(b => b.key);
        const currentBlockKey = targetEl.getAttribute('data-offset-key').substr(0, 5);
        if (contentBlockKeys.includes(currentBlockKey)) {
            contentBlocks.forEach(block => {
                if (block.key === currentBlockKey) {
                    block.data['hover'] = highlightStateIsOn;
                }
            });

            const contentState = convertFromRaw({ "blocks": contentBlocks, "entityMap": {} });
            setState({
                editorState: EditorState.push(editorState, contentState, 'change-block-data')
            });
        }
    }
}

export const markBlockComplete = (editorState, setState) => {
    const currentContent = editorState.getCurrentContent();
    const selectionState = editorState.getSelection();
    const selectionIsForward = !selectionState.isBackward;

    const selectionAnchorKey = selectionIsForward ? selectionState.getAnchorKey() : selectionState.getFocusKey();
    const selectionFocusKey = selectionIsForward ? selectionState.getFocusKey() : selectionState.getAnchorKey();

    // set starting key and offset
    let firstBlockSelected = false;
    let startingBlockKey = currentContent.getKeyBefore(selectionAnchorKey);
    if (!startingBlockKey) {
        firstBlockSelected = true;
        startingBlockKey = selectionAnchorKey;
    }
    const startingBlockOffset = !firstBlockSelected ? currentContent.getBlockForKey(startingBlockKey).getLength() : 0;

    // set last key and offset
    let lastBlockKey, lastBlockOffset;
    const lastBlockSelected = !currentContent.getKeyAfter(selectionFocusKey);
    // edge case for both first and last selected
    if (firstBlockSelected && lastBlockSelected) {
        lastBlockKey = selectionFocusKey;
        lastBlockOffset = currentContent.getBlockForKey(lastBlockKey).getLength();
    } else {
        // when first block is selected next block is needed else first block comes as empty
        lastBlockKey = firstBlockSelected ? currentContent.getKeyAfter(selectionFocusKey) : selectionFocusKey;
        lastBlockOffset = firstBlockSelected ? 0 : currentContent.getBlockForKey(lastBlockKey).getLength();
    }

    const removeSelection = new SelectionState({
        anchorKey: startingBlockKey,
        anchorOffset: startingBlockOffset,
        focusKey: lastBlockKey,
        focusOffset: lastBlockOffset
    });
    const stateWithRemovedBlocks = EditorState.push(
        editorState,
        Modifier.removeRange(currentContent, removeSelection, 'forward'),
        'remove-range'
    );

    // todo: selection not working properly atm

    // setting new selection
    let blockToFocus, blockToFocusOffset;
    if (lastBlockSelected) {
        // if last block is selected.. previous should get focus.. it is at end for now
        blockToFocus = startingBlockKey;
        blockToFocusOffset = currentContent.getBlockForKey(blockToFocus).getLength();
        // set offset also
    } else {
        // normally the next block should get cursor
        blockToFocus = lastBlockKey;
        blockToFocusOffset = 0;
    }

    const newSelection = SelectionState.createEmpty(blockToFocus)
        .merge({
            anchorKey: blockToFocus,
            anchorOffset: blockToFocusOffset,
            focusKey: blockToFocus,
            focusOffset: blockToFocusOffset,
            hasFocus: true
        });

    setState({ editorState: EditorState.acceptSelection(stateWithRemovedBlocks, newSelection) });
}
