import { convertToRaw, convertFromRaw, ContentState, EditorState } from 'draft-js';
import { defaultBlockContent } from '../constants';

export const convertItemListToContentState = (itemList) => {
    if (!itemList || itemList.length === 0) return ContentState.createFromText('');

    const formattedItemList = itemList.map(t => {
        return {
            key: t.id,
            text: t.text,
            ...defaultBlockContent
        }
    });
    return convertFromRaw({ 'blocks': formattedItemList, 'entityMap': {} });
}

export const calculateEditorStateFromItemList = (itemList) => {
    const contentState = convertItemListToContentState(itemList);
    return EditorState.createWithContent(contentState);
}

export const convertContentStateToItemList = (contentState) => {
    const blockList = convertToRaw(contentState).blocks;
    return blockList.map(b => {
        return {
            id: b.id, text: b.text, parentKey: b.parentKey
        }
    });
}

export const generatePathArray = (itemList, parentKey) => {
    const pathArray = []; // javascript array can work as a stack
    let currentParentKey = parentKey;
    let parentTodo = itemList.find(t => t.id === currentParentKey);
    while (parentTodo) {
        currentParentKey = parentTodo.parentKey;
        pathArray.push({ id: parentTodo.id, text: parentTodo.text });
        // eslint-disable-next-line
        parentTodo = itemList.find(t => t.id === currentParentKey);
    }
    return pathArray.reverse();
}
