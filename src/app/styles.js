// App.js
export const appStyle = { width: 600, margin: '0 auto', marginTop: 100 };

// Editor.js
export const topDivStyle = { backgroundColor: 'wheat', border: '1px solid black' }; // todo: move styling out

// Breadcrumb.js
export const breadcrumbStyle = { padding: 10, borderRadius: 50, display: 'inline', backgroundColor: 'rgb(192, 229, 192)' };
