import React from 'react';
import { breadcrumbStyle } from '../styles';

const Breadcrumb = ({ pathArray, setParentKey }) => {
    const pathDivs = pathArray.map((path, i) =>
        <span key={i}>
            <span className="pathname" onClick={() => setParentKey(path.id)}>{path.text}</span>
            {i < pathArray.length - 1 ? <span>-></span> : <span />}
        </span>
    );

    return (
        <div style={breadcrumbStyle}>
            <span>Path: </span>
            {pathDivs}
        </div>
    )
};

export default Breadcrumb;
