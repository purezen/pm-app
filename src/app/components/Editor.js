import React, { Component, createRef } from 'react';
import {
    Editor, KeyBindingUtil, getDefaultKeyBinding
} from 'draft-js';

import { markBlockComplete, setHighlightBlockHandle } from '../utils/userInteractions';
import { calculateEditorStateFromItemList } from '../utils/utils';

import { keyCodeForReturnKey } from '../constants';
import { topDivStyle } from '../styles';

class EditorComp extends Component {
    // NOTE: don't intend to handle editor state outside of 
    // this component.. or any draft method
    constructor(props) {
        super(props);

        this.state = {
            itemList: props.itemList,
            editorState: calculateEditorStateFromItemList(props.itemList)
        };

        this.domEditor = createRef();

        this.bindFunctionsToClassThis();
    }

    componentDidMount() {
        this.domEditor.current.focus(); // sets focus on load
    }

    static getDerivedStateFromProps(props, state) {
        if (props.itemList !== state.itemList) {
            // updates editor state when new list is received from parent comp
            return {
                itemList: props.itemList,
                editorState: calculateEditorStateFromItemList(props.itemList)
            };
        } else
            return state;
    }

    onEditorUpdate(editorState) {
        this.setState({ editorState });
    }

    keyBindingFn(e) {
        const { hasCommandModifier } = KeyBindingUtil;

        if (e.keyCode === keyCodeForReturnKey && hasCommandModifier(e)) {
            return 'mark-todo-complete'
        }

        return getDefaultKeyBinding(e);
    }

    handleKeyCommand(command, editorState) {
        switch (command) {
            case 'mark-todo-complete':
                markBlockComplete(editorState, this.setState.bind(this));
                return 'handled';
            default:
                return 'not-handled';
        }
    }

    blockStyleFn(block) {
        const blockHasHover = block.getData().toJS()['hover'];

        if (blockHasHover) {
            return 'block-hover';
        } else {
            return 'block';
        }
    }

    onMouseOver(e) {
        setHighlightBlockHandle(e, true, this.state.editorState, this.setState.bind(this))
    }

    onMouseOut(e) {
        setHighlightBlockHandle(e, false, this.state.editorState, this.setState.bind(this))
    }

    render() {
        const { editorState } = this.state;

        return (
            <div
                onMouseOver={this.onMouseOver}
                onMouseOut={this.onMouseOut}
                style={topDivStyle}>

                <Editor
                    ref={this.domEditor}
                    editorState={editorState}
                    onChange={this.onEditorUpdate}
                    blockStyleFn={this.blockStyleFn}
                    keyBindingFn={this.keyBindingFn}
                    handleKeyCommand={this.handleKeyCommand}
                />
            </div>
        )
    }

    bindFunctionsToClassThis() {
        this.onEditorUpdate = this.onEditorUpdate.bind(this);
        this.blockStyleFn = this.blockStyleFn.bind(this);
        this.keyBindingFn = this.keyBindingFn.bind(this);
        this.handleKeyCommand = this.handleKeyCommand.bind(this);
        this.onMouseOver = this.onMouseOver.bind(this);
        this.onMouseOut = this.onMouseOut.bind(this);
    }
};

export default EditorComp;
